

Projects = new Meteor.Collection "Projects"
Features = new Meteor.Collection "Features"
Tasks = new Meteor.Collection "Tasks"

Views = [
	{ "title": "Overview", "view": "overview" },
	{ "title": "Features", "view": "features" },
	{ "title": "Notes",    "view": "notes" },
	{ "title": "Right Now", "view": "right_now" }
]


fauxTask = (f_id, nonsense) ->
	feature: f_id
	title: nonsense.words(6)
	progress: nonsense.frac() * 100
	effort:
		estimated: nonsense.integerInRange(0,10)
		current: Math.round(nonsense.realInRange(1,11) / 0.5) * 0.5

fauxFeature = (p_id, nonsense) ->
	project: p_id
	title: nonsense.buzzPhrase()
	description: nonsense.sentences()

fauxProject = (nonsense) ->
	tmp = nonsense.jobTitle()

	title: tmp
	slug: tmp.replace ' ', '-'
	status: nonsense.pick [ 'planning', 'active', 'complete', 'cancelled' ]
	color: "rgb( #{nonsense.integerInRange(20,240)}, #{nonsense.integerInRange(20,240)}, #{nonsense.integerInRange(20,240)})"


# ----------------------------
#   CLIENT
# ----------------------------
if Meteor.isClient

	App =
		state:
			p_id: amplify.store 'p_id'
			view: amplify.store 'view'

	App.setProject = (p_id) ->
		amplify.store 'p_id', p_id
		Session.set 'current_project', p_id
		@_project = Projects.findOne p_id

	App.getProject = () ->
		@_project

	App.setView = (view) ->
		amplify.store 'view', view
		Session.set 'current_view', view
		@_view = view

	App.getView = () ->
		@_view


	# - - - Initial Load - - -
	# ------------------------
	Meteor.startup ->

		view = 'welcome'
		if App.state.p_id
			Session.set 'current_project', App.state.p_id
			view = 'overview'
		if App.state.view
			Session.set 'current_view', App.state.view
			view = App.state.view
		content = Meteor.render Template[view]
		$('#main').html content


	# - - - Header - - -
	# ------------------
	Template.header.views = Views

	Template.header.is_active_view = () ->
		if Session.equals "current_view", @view
			'active'
	
	Template.header.is_current_project = () ->
		if Session.equals "current_project", @_id
			'active'
	
	Template.header.current_project = () ->
		id = Session.get 'current_project'
		if id
			Projects.findOne id
		else
			title: "Select a project"
	
	Template.header.project_color = () ->
		if App.getProject()
			App.getProject().color
	
	Template.header.active_projects = () ->
		Projects.find { status: "active" }

	Template.header.events
		'click a[data-view]' : (e) ->
			view = e.target.getAttribute 'data-view'
			if view and Template.hasOwnProperty view
				main = $('#main')
				content = Meteor.render Template[view]
				main.html content
				# Session.set 'current_view', view
				App.setView view
		'click a[data-project]' : (e) ->
			# Session.set 'current_project', @_id
			App.setProject @_id


	# - - - Projects - - -
	# --------------------
	Template.projects.projects = () ->
		active: Projects.find( { status: "active" } )
		archived: Projects.find( { status: "archived" } )

	Template.projects.is_active = () ->
		if Session.equals "selected_project", @_id
			"active"

	Template.projects.events
		'click .select-project' : () ->
			Session.set "selected_project", @_id


	# - - - Welcome - - -
	# --------------------
	Template.welcome.projects = () ->
		Projects.find( { status: "active" } )

	Template.welcome.events
		'click .select-project' : () ->
			Session.set "current_project", @_id
			Session.set "current_view", "overview"


	# - - - Overview - - -
	# --------------------
	Template.overview.project = () ->
		p_id = Session.get 'current_project'
		if p_id
			Projects.findOne p_id
		else
			title: "Select a project"

	Template.overview.features = () ->
		p_id = Session.get "current_project"
		Features.find { project: p_id }

	Template.overview.stats = () ->
		p_id = Session.get "current_project"
		features = Features.find { project: p_id }
		feature_count: features.count()


	# - - - Features - - -
	# --------------------
	Template.features.features = () ->
		p_id = Session.get "current_project"
		Features.find { project: p_id }

	Template.features.selected_feature = () ->
		f_id = Session.get "selected_feature"
		Features.findOne f_id

	Template.features.tasks = () ->
		f_id = Session.get "selected_feature"
		Tasks.find { feature: f_id }


	# - - - Feature Blocks - - -
	# --------------------------
	Template.feature_block.selected = () ->
		if Session.equals "selected_feature", @_id
			"selected"

	Template.feature_block.events
		'click .title' : () ->
			Session.set "selected_feature", @_id


