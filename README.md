Trackadactyl
============
	The `p` is silent (and non-existent).

The ideas are still taking shape but imagine if agile/scrum and GTD had a child and then it was bitten by a radio-active kanban board. Now, try to pretend that this freak-baby only inherited the awesome bits. That's kind of what we're aiming for.

Basically Tracadactyl totally wants to be your BFF with benefits. Specifically, project & task management benefits.


Principles
----------
* Context should be distributed with care
* Info out of context is distracting
* Friction is evil and brings out the worst in everyone


### Dev Instructions
1. Install Meteor & Meteorite.
2. Clone this repository.
3. Run `mrt` from within the project directory.
4. Build awesome.
